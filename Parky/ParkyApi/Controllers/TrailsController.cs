﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Controllers
{
    //[Route("api/Trails")]
    [Route("api/v{version:apiVersion}/trails")]
    [ApiController]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    //[ApiExplorerSettings(GroupName = "TrailsAPI")]
    public class TrailsController : ControllerBase
    {
        private readonly ITrailRepository _trailRepo;
        private readonly IMapper _mapper;

        public TrailsController(ITrailRepository trailRepo, IMapper mapper)
        {
            _trailRepo = trailRepo;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all record of NationalPark table
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<TrailDto>))]
        public IActionResult GetTrails()
        {
            var objList = _trailRepo.GetTrails();
            //Mapping entity to DTO to public
            var objDtoList = new List<TrailDto>();
            foreach (var item in objList)
            {
                objDtoList.Add(_mapper.Map<TrailDto>(item));
            }

            return Ok(objDtoList);
        }

        /// <summary>
        /// Get Trail by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}", Name = "GetTrail")]
        [ProducesResponseType(200, Type = typeof(TrailDto))]
        [ProducesResponseType(404)]
        [ProducesDefaultResponseType]
        [Authorize(Roles = "Admin")]
        public IActionResult GetTrail(int id)
        {
            var obj = _trailRepo.GetTrail(id);
             
            if (obj == null)
                return NotFound();

            var objDto = _mapper.Map<TrailDto>(obj);

            return Ok(objDto); 

        }

        /// <summary>
        /// Get List of trail by national Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getTrails/{NationalParkId:int}", Name = "GetTrailInNationalPark")]
        [ProducesResponseType(200, Type = typeof(TrailDto))]
        [ProducesResponseType(404)]
        [ProducesDefaultResponseType]
        public IActionResult GetTrailInNationPark(int NationalParkId)
        {
            var objList = _trailRepo.GetTrailsInNationalPark(NationalParkId);

            if (objList == null)
                return NotFound();

            var objDto = new List<TrailDto>();
            foreach (var item in objList)
            {
                objDto.Add(_mapper.Map<TrailDto>(item));
            }
            

            return Ok(objDto);

        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(TrailDto))]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult CreateTrail([FromBody] TrailCreateDto trailDto)
        {
            if (trailDto == null)
                return BadRequest(ModelState);

            if (_trailRepo.TrailExists(trailDto.Name))
            {
                ModelState.AddModelError("", "Trail Existed!");
                return StatusCode(404, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var trail = _mapper.Map<Trail>(trailDto);

            try
            {
                if (_trailRepo.CreateTrail(trail))
                {
                    return CreatedAtRoute("GetTrail", new { id = trail.Id }, trail);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", $"Something Went Wrong when saving {trail.Name}." + ex.Message);
            }

            return StatusCode(500, ModelState);
            //return Ok();
        }


        [HttpPatch("{id:int}", Name = "UpdateTrail")]
        [ProducesResponseType(204)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult UpdateTrail(int id, [FromBody] TrailUpdateDto trailDto)
        {
            if (trailDto == null || id != trailDto.Id)
                return BadRequest(ModelState);

            var trail = _mapper.Map<Trail>(trailDto);

            try
            {
                if (_trailRepo.UpdateTrail(trail))
                {
                    return CreatedAtRoute("GetTrail", new { id = trail.Id }, trail);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", $"Something Went Wrong when saving {trail.Name}." + ex.Message);
                return StatusCode(500, ModelState);
            }

            

            return NoContent();
        }


        [HttpDelete("{id:int}", Name = "DeleteTrail")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult DeleteTrail(int id)
        {
            if (!_trailRepo.TrailExists(id))
                return NotFound();

            var trail = _trailRepo.GetTrail(id);

            if (!_trailRepo.DeleteTrail(trail))
            {
                ModelState.AddModelError("", $"Something Went Wrong when Deleting {trail.Name}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
