﻿using WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Repository.IRepository
{
    public interface ITrailRepository : IRepository<Trail>
    {
    }
}
